<?php

namespace Webforia_Checkout_Fields;

class Plugin_Init {

    public function __construct() {
        add_action('wp_enqueue_scripts', [$this, 'scripts']);
        add_action('plugins_loaded', [$this, 'load_plugin_textdomain']);
        add_filter('body_class', [$this, 'add_body_class']);
    }

    /**
     * Scripts
     *
     * @return void
     */
    public function scripts() {
        if (is_checkout()) {
            wp_enqueue_style('wcf-style', WEBFORIA_CHECKOUT_FIELD_ASSETS . 'assets/css/checkout-field.min.css', '', '1.5.0');
            wp_enqueue_script('wcf-script', WEBFORIA_CHECKOUT_FIELD_ASSETS . 'assets/js/checkout-field.min.js', array('animate'), '1.5.0', true);
        }
    }

    /**
     * Added body css class
     *
     * @param [type] $classes css class
     * @return void
     */
    public function add_body_class($classes) {
        $classes[] = 'webforia-checkout-field';

        if (function_exists('woongkir_autoload')) {

            $classes[] = 'wooongkir';
        }

        if (function_exists('run_ongkoskirim_id')) {

            $classes[] = 'ongkoskirim-id';
        }

        if (function_exists('Plugin_Ongkos_Kirim')) {

            $classes[] = 'plugin-ongkor-kirim';
        }


        return $classes;
    }

    /**
     * Load plugin domain pot
     *
     * @return void
     */
    public function load_plugin_textdomain() {
        load_plugin_textdomain(WEBFORIA_CHECKOUT_FIELD_DOMAIN, false, dirname(plugin_basename(__FILE__)) . '/languages/');
    }
}
